module.exports = (
  /** @type {import('plop').NodePlopAPI} */
  plop
) => {
  plop.setGenerator("component", {
    description: "Create a reusable component",
    prompts: [
      {
        type: "input",
        name: "name",
        message: "What is your component name?",
      },
      {
        type: "confirm",
        name: "hasTests",
        message: "Does this components have tests?",
        default: false,
      },
      {
        type: "input",
        name: "directory",
        message: "Where should the component be created?",
        default: "src/components",
      },
    ],
    actions: [
      {
        type: "add",
        path: "{{directory}}/{{pascalCase name}}/{{pascalCase name}}.tsx",
        templateFile: "plop-templates/Component/Component.tsx.hbs",
      },
      {
        type: "add",
        path: "{{directory}}/{{pascalCase name}}/__tests__/{{pascalCase name}}.test.tsx",
        templateFile:
          "plop-templates/Component/__tests__/Component.test.tsx.hbs",
        skip: ({ hasTests }) => {
          return hasTests || "tests not requested for this component!!!";
        },
      },
      {
        type: "add",
        path: "{{directory}}/{{pascalCase name}}/{{pascalCase name}}.stories.tsx",
        templateFile: "plop-templates/Component/Component.stories.tsx.hbs",
      },
      {
        type: "add",
        path: "{{directory}}/{{pascalCase name}}/index.ts",
        templateFile: "plop-templates/Component/index.ts.hbs",
      },
    ],
  });
};
