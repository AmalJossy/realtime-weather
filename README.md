# Realtime weather app

... react app that shows some fake weather data

## Installation

Typical npm project

```bash
  npm install
```

## Run Locally

Clone the project

```bash
  git clone git@gitlab.com:AmalJossy/realtime-weather.git
```

Go to the project directory

```bash
  cd realtime-weather
```

Install dependencies

```bash
  npm install
```

1. Build and run

- Build the app

  ```bash
  npm run build
  ```

- Run the app

  ```bash
  npm run start
  ```

2. Run in dev mode

```bash
  npm run dev
```

3. Run Storybook

```bash
  npm run storybook
```

## Acknowledgements

- Only few sections show real world data, most apis mimic something similar to openweathemap response
- Realtime data via sockets are entirely mocked
  - Updates every 10 seconds, intervals can be changed in code and the charts will still work
- Uses socket io, next server has issues supporting websockets so socket fails to upgrade to proper websockets, please consider this a mock setup, the upgrade will happen when using a proper websocket server
