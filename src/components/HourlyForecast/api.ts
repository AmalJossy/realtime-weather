import apiMap from '@/constants/apiMap';
import { WeatherForecastResponse } from '@/types/apiResponses/hourlyForecast';
import { CoordinateTuple } from '@/types/weather';

export const getHourlyForecast = async (coordinates: CoordinateTuple) => {
  const response = await fetch(
    `${apiMap.HOURLY_FORECAST}?lat=${coordinates[0]}&lon=${coordinates[1]}`,
  );
  const data: WeatherForecastResponse = await response.json();
  return data;
};
