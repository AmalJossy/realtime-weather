import { WeatherForecastResponse } from '@/types/apiResponses/hourlyForecast';
import { useEffect, useState } from 'react';
import HourTile from './HourTile';
import { CityGeoData } from '@/types/apiResponses/geoSearch';
import { WeatherForecast } from '@/types/weather';
import { useTemperatureUnit } from '@/contexts/tempUnitContext';
import { TemperatureUnit } from '@/constants/weather';
import { kelvinToUnitStr } from '@/utils/display';
import { getHourlyForecast } from './api';

const getWeatherSummary = (
  forecast: WeatherForecast,
  unit: TemperatureUnit,
) => {
  const temperature = kelvinToUnitStr(forecast.main.temp, unit);
  const humidity = Math.floor(forecast.main.humidity);
  const windSpeed = Math.floor((forecast.wind.speed * 3600) / 1000);
  return {
    temperature,
    humidity,
    windSpeed,
  };
};

type HourlyForecastProps = {
  city: CityGeoData;
};
const HourlyForecast = ({ city }: HourlyForecastProps) => {
  const [forecasts, setForecasts] = useState<WeatherForecastResponse>();
  const [isLoading, setIsLoading] = useState(true);

  const { unit } = useTemperatureUnit();

  useEffect(() => {
    const fetchData = async () => {
      setIsLoading(true);
      setForecasts(await getHourlyForecast(city.coordinates));
      setIsLoading(false);
    };
    fetchData();
  }, [city]);

  if (!forecasts || isLoading) return null;

  const currentWeather = forecasts.list[0];
  const summary = getWeatherSummary(currentWeather, unit);

  return (
    <div className="rounded-2xl bg-gray-100 p-4">
      <div className="flex flex-col justify-between md:flex-row">
        <div className="flex items-center">
          <img
            src={`http://openweathermap.org/img/wn/${currentWeather.weather[0].icon}@2x.png`}
            alt={currentWeather.weather[0].main}
          />
          <div className="mx-4">
            <div className="text-4xl font-semibold">{city.name}</div>
            <div>{city.country}</div>
          </div>
        </div>
        <div className="flex flex-col items-center gap-4 self-end sm:flex-row">
          <div>
            <div className="text-4xl">{summary.temperature}</div>
            <div>Temperature</div>
          </div>
          <div>
            <div className="text-4xl">
              {summary.humidity}
              <span className="text-sm">%</span>
            </div>
            <div>Humidity</div>
          </div>
          <div>
            <div className="text-4xl">
              {summary.windSpeed}
              <span className="text-sm">km/h</span>
            </div>
            <div>Wind Speed</div>
          </div>
        </div>
      </div>
      <div className="flex gap-4 overflow-y-auto py-4">
        {forecasts.list.map((forecast) => (
          <HourTile
            key={forecast.time}
            icon={forecast.weather[0].icon}
            weather={forecast.weather[0].description}
            temperature={forecast.main.temp}
            time={forecast.time}
          />
        ))}
      </div>
    </div>
  );
};
export default HourlyForecast;
