import { useTemperatureUnit } from '@/contexts/tempUnitContext';
import { kelvinToUnitStr } from '@/utils/display';

type HourTileProps = {
  weather: string;
  time: string;
  icon: string;
  temperature: number;
};
const getAmPm = (time: string) => {
  const date = new Date(time);
  const hour = date.getHours();
  if (hour === 0) return `12 am`;
  if (hour === 12) return `12 pm`;
  if (hour > 12) return `${hour - 12} pm`;
  return `${hour} am`;
};

const HourTile = ({ time, icon, temperature, weather }: HourTileProps) => {
  const { unit } = useTemperatureUnit();
  return (
    <div className="w-24 shrink-0 rounded-xl bg-gray-200 p-4 text-center">
      <div className="text-sm">{getAmPm(time)}</div>
      <img
        src={`http://openweathermap.org/img/wn/${icon}@2x.png`}
        alt={weather}
        className="mx-auto h-12"
      />
      <div className="font-medium">{kelvinToUnitStr(temperature, unit)}</div>
    </div>
  );
};

export default HourTile;
