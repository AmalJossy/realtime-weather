import type { Meta, StoryObj } from '@storybook/react';
import mockHandlers from './api.mock';

import HourlyForecast from '.';

export default {
  title: 'Common/HourlyForecast',
  component: HourlyForecast,
  parameters: {
    msw: mockHandlers,
  },
} as Meta<typeof HourlyForecast>;
type Story = StoryObj<typeof HourlyForecast>;

export const Basic: Story = {
  args: {
    city: {
      name: 'Berlin',
      country: 'Germany',
      coordinates: [52.52, 13.405],
    },
  },
};
