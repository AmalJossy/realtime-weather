import apiMap from '@/constants/apiMap';
import { rest } from 'msw';

export const mockGetHourlyForecast = rest.get(
  `*${apiMap.HOURLY_FORECAST}`,
  async (_, res, ctx) => {
    return res(
      ctx.status(200),
      ctx.json({
        cod: '200',
        message: 0,
        cnt: 96,
        list: [
          {
            dt: 1661878800,
            main: {
              temp: 296.31,
              feels_like: 296.07,
              temp_min: 296.2,
              temp_max: 296.31,
              pressure: 1015,
              sea_level: 1015,
              grnd_level: 932,
              humidity: 53,
              temp_kf: 0.11,
            },
            weather: [
              {
                id: 500,
                main: 'Rain',
                description: 'light rain',
                icon: '10d',
              },
            ],
            clouds: {
              all: 95,
            },
            wind: {
              speed: 1.58,
              deg: 103,
              gust: 3.52,
            },
            visibility: 10000,
            pop: 0.4,
            rain: {
              '1h': 0.24,
            },
            sys: {
              pod: 'd',
            },
            dt_txt: '2022-08-30 17:00:00',
            time: '2023-10-29T13:57:52.313Z',
          },
          {
            dt: 1661882400,
            main: {
              temp: 294.94,
              feels_like: 294.74,
              temp_min: 292.84,
              temp_max: 294.94,
              pressure: 1015,
              sea_level: 1015,
              grnd_level: 931,
              humidity: 60,
              temp_kf: 2.1,
            },
            weather: [
              {
                id: 500,
                main: 'Rain',
                description: 'light rain',
                icon: '10n',
              },
            ],
            clouds: {
              all: 93,
            },
            wind: {
              speed: 1.97,
              deg: 157,
              gust: 3.39,
            },
            visibility: 10000,
            pop: 0.33,
            rain: {
              '1h': 0.2,
            },
            sys: {
              pod: 'n',
            },
            dt_txt: '2022-08-30 18:00:00',
            time: '2023-10-29T14:57:52.313Z',
          },
          {
            dt: 1661875200,
            main: {
              temp: 296.34,
              feels_like: 296.02,
              temp_min: 296.34,
              temp_max: 298.24,
              pressure: 1015,
              sea_level: 1015,
              grnd_level: 933,
              humidity: 50,
              temp_kf: -1.9,
            },
            weather: [
              {
                id: 500,
                main: 'Rain',
                description: 'light rain',
                icon: '10d',
              },
            ],
            clouds: {
              all: 97,
            },
            wind: {
              speed: 1.06,
              deg: 66,
              gust: 2.16,
            },
            visibility: 10000,
            pop: 0.32,
            rain: {
              '1h': 0.13,
            },
            sys: {
              pod: 'd',
            },
            dt_txt: '2022-08-30 16:00:00',
            time: '2023-10-29T15:57:52.313Z',
          },
          {
            dt: 1661878800,
            main: {
              temp: 296.31,
              feels_like: 296.07,
              temp_min: 296.2,
              temp_max: 296.31,
              pressure: 1015,
              sea_level: 1015,
              grnd_level: 932,
              humidity: 53,
              temp_kf: 0.11,
            },
            weather: [
              {
                id: 500,
                main: 'Rain',
                description: 'light rain',
                icon: '10d',
              },
            ],
            clouds: {
              all: 95,
            },
            wind: {
              speed: 1.58,
              deg: 103,
              gust: 3.52,
            },
            visibility: 10000,
            pop: 0.4,
            rain: {
              '1h': 0.24,
            },
            sys: {
              pod: 'd',
            },
            dt_txt: '2022-08-30 17:00:00',
            time: '2023-10-29T16:57:52.313Z',
          },
          {
            dt: 1661875200,
            main: {
              temp: 296.34,
              feels_like: 296.02,
              temp_min: 296.34,
              temp_max: 298.24,
              pressure: 1015,
              sea_level: 1015,
              grnd_level: 933,
              humidity: 50,
              temp_kf: -1.9,
            },
            weather: [
              {
                id: 500,
                main: 'Rain',
                description: 'light rain',
                icon: '10d',
              },
            ],
            clouds: {
              all: 97,
            },
            wind: {
              speed: 1.06,
              deg: 66,
              gust: 2.16,
            },
            visibility: 10000,
            pop: 0.32,
            rain: {
              '1h': 0.13,
            },
            sys: {
              pod: 'd',
            },
            dt_txt: '2022-08-30 16:00:00',
            time: '2023-10-29T17:57:52.313Z',
          },
          {
            dt: 1661875200,
            main: {
              temp: 296.34,
              feels_like: 296.02,
              temp_min: 296.34,
              temp_max: 298.24,
              pressure: 1015,
              sea_level: 1015,
              grnd_level: 933,
              humidity: 50,
              temp_kf: -1.9,
            },
            weather: [
              {
                id: 500,
                main: 'Rain',
                description: 'light rain',
                icon: '10d',
              },
            ],
            clouds: {
              all: 97,
            },
            wind: {
              speed: 1.06,
              deg: 66,
              gust: 2.16,
            },
            visibility: 10000,
            pop: 0.32,
            rain: {
              '1h': 0.13,
            },
            sys: {
              pod: 'd',
            },
            dt_txt: '2022-08-30 16:00:00',
            time: '2023-10-29T18:57:52.313Z',
          },
          {
            dt: 1661875200,
            main: {
              temp: 296.34,
              feels_like: 296.02,
              temp_min: 296.34,
              temp_max: 298.24,
              pressure: 1015,
              sea_level: 1015,
              grnd_level: 933,
              humidity: 50,
              temp_kf: -1.9,
            },
            weather: [
              {
                id: 500,
                main: 'Rain',
                description: 'light rain',
                icon: '10d',
              },
            ],
            clouds: {
              all: 97,
            },
            wind: {
              speed: 1.06,
              deg: 66,
              gust: 2.16,
            },
            visibility: 10000,
            pop: 0.32,
            rain: {
              '1h': 0.13,
            },
            sys: {
              pod: 'd',
            },
            dt_txt: '2022-08-30 16:00:00',
            time: '2023-10-29T19:57:52.313Z',
          },
          {
            dt: 1661882400,
            main: {
              temp: 294.94,
              feels_like: 294.74,
              temp_min: 292.84,
              temp_max: 294.94,
              pressure: 1015,
              sea_level: 1015,
              grnd_level: 931,
              humidity: 60,
              temp_kf: 2.1,
            },
            weather: [
              {
                id: 500,
                main: 'Rain',
                description: 'light rain',
                icon: '10n',
              },
            ],
            clouds: {
              all: 93,
            },
            wind: {
              speed: 1.97,
              deg: 157,
              gust: 3.39,
            },
            visibility: 10000,
            pop: 0.33,
            rain: {
              '1h': 0.2,
            },
            sys: {
              pod: 'n',
            },
            dt_txt: '2022-08-30 18:00:00',
            time: '2023-10-29T20:57:52.313Z',
          },
          {
            dt: 1661875200,
            main: {
              temp: 296.34,
              feels_like: 296.02,
              temp_min: 296.34,
              temp_max: 298.24,
              pressure: 1015,
              sea_level: 1015,
              grnd_level: 933,
              humidity: 50,
              temp_kf: -1.9,
            },
            weather: [
              {
                id: 500,
                main: 'Rain',
                description: 'light rain',
                icon: '10d',
              },
            ],
            clouds: {
              all: 97,
            },
            wind: {
              speed: 1.06,
              deg: 66,
              gust: 2.16,
            },
            visibility: 10000,
            pop: 0.32,
            rain: {
              '1h': 0.13,
            },
            sys: {
              pod: 'd',
            },
            dt_txt: '2022-08-30 16:00:00',
            time: '2023-10-29T21:57:52.313Z',
          },
          {
            dt: 1661878800,
            main: {
              temp: 296.31,
              feels_like: 296.07,
              temp_min: 296.2,
              temp_max: 296.31,
              pressure: 1015,
              sea_level: 1015,
              grnd_level: 932,
              humidity: 53,
              temp_kf: 0.11,
            },
            weather: [
              {
                id: 500,
                main: 'Rain',
                description: 'light rain',
                icon: '10d',
              },
            ],
            clouds: {
              all: 95,
            },
            wind: {
              speed: 1.58,
              deg: 103,
              gust: 3.52,
            },
            visibility: 10000,
            pop: 0.4,
            rain: {
              '1h': 0.24,
            },
            sys: {
              pod: 'd',
            },
            dt_txt: '2022-08-30 17:00:00',
            time: '2023-10-29T22:57:52.313Z',
          },
          {
            dt: 1662217200,
            main: {
              temp: 294.14,
              feels_like: 293.99,
              temp_min: 294.14,
              temp_max: 294.14,
              pressure: 1014,
              sea_level: 1014,
              grnd_level: 931,
              humidity: 65,
              temp_kf: 0,
            },
            weather: [
              {
                id: 804,
                main: 'Clouds',
                description: 'overcast clouds',
                icon: '04d',
              },
            ],
            clouds: {
              all: 100,
            },
            wind: {
              speed: 0.91,
              deg: 104,
              gust: 1.92,
            },
            visibility: 10000,
            pop: 0.53,
            sys: {
              pod: 'd',
            },
            dt_txt: '2022-09-03 15:00:00',
            time: '2023-10-29T23:57:52.313Z',
          },
          {
            dt: 1661882400,
            main: {
              temp: 294.94,
              feels_like: 294.74,
              temp_min: 292.84,
              temp_max: 294.94,
              pressure: 1015,
              sea_level: 1015,
              grnd_level: 931,
              humidity: 60,
              temp_kf: 2.1,
            },
            weather: [
              {
                id: 500,
                main: 'Rain',
                description: 'light rain',
                icon: '10n',
              },
            ],
            clouds: {
              all: 93,
            },
            wind: {
              speed: 1.97,
              deg: 157,
              gust: 3.39,
            },
            visibility: 10000,
            pop: 0.33,
            rain: {
              '1h': 0.2,
            },
            sys: {
              pod: 'n',
            },
            dt_txt: '2022-08-30 18:00:00',
            time: '2023-10-30T00:57:52.313Z',
          },
          {
            dt: 1661882400,
            main: {
              temp: 294.94,
              feels_like: 294.74,
              temp_min: 292.84,
              temp_max: 294.94,
              pressure: 1015,
              sea_level: 1015,
              grnd_level: 931,
              humidity: 60,
              temp_kf: 2.1,
            },
            weather: [
              {
                id: 500,
                main: 'Rain',
                description: 'light rain',
                icon: '10n',
              },
            ],
            clouds: {
              all: 93,
            },
            wind: {
              speed: 1.97,
              deg: 157,
              gust: 3.39,
            },
            visibility: 10000,
            pop: 0.33,
            rain: {
              '1h': 0.2,
            },
            sys: {
              pod: 'n',
            },
            dt_txt: '2022-08-30 18:00:00',
            time: '2023-10-30T01:57:52.313Z',
          },
          {
            dt: 1661878800,
            main: {
              temp: 296.31,
              feels_like: 296.07,
              temp_min: 296.2,
              temp_max: 296.31,
              pressure: 1015,
              sea_level: 1015,
              grnd_level: 932,
              humidity: 53,
              temp_kf: 0.11,
            },
            weather: [
              {
                id: 500,
                main: 'Rain',
                description: 'light rain',
                icon: '10d',
              },
            ],
            clouds: {
              all: 95,
            },
            wind: {
              speed: 1.58,
              deg: 103,
              gust: 3.52,
            },
            visibility: 10000,
            pop: 0.4,
            rain: {
              '1h': 0.24,
            },
            sys: {
              pod: 'd',
            },
            dt_txt: '2022-08-30 17:00:00',
            time: '2023-10-30T02:57:52.313Z',
          },
          {
            dt: 1661875200,
            main: {
              temp: 296.34,
              feels_like: 296.02,
              temp_min: 296.34,
              temp_max: 298.24,
              pressure: 1015,
              sea_level: 1015,
              grnd_level: 933,
              humidity: 50,
              temp_kf: -1.9,
            },
            weather: [
              {
                id: 500,
                main: 'Rain',
                description: 'light rain',
                icon: '10d',
              },
            ],
            clouds: {
              all: 97,
            },
            wind: {
              speed: 1.06,
              deg: 66,
              gust: 2.16,
            },
            visibility: 10000,
            pop: 0.32,
            rain: {
              '1h': 0.13,
            },
            sys: {
              pod: 'd',
            },
            dt_txt: '2022-08-30 16:00:00',
            time: '2023-10-30T03:57:52.313Z',
          },
          {
            dt: 1661875200,
            main: {
              temp: 296.34,
              feels_like: 296.02,
              temp_min: 296.34,
              temp_max: 298.24,
              pressure: 1015,
              sea_level: 1015,
              grnd_level: 933,
              humidity: 50,
              temp_kf: -1.9,
            },
            weather: [
              {
                id: 500,
                main: 'Rain',
                description: 'light rain',
                icon: '10d',
              },
            ],
            clouds: {
              all: 97,
            },
            wind: {
              speed: 1.06,
              deg: 66,
              gust: 2.16,
            },
            visibility: 10000,
            pop: 0.32,
            rain: {
              '1h': 0.13,
            },
            sys: {
              pod: 'd',
            },
            dt_txt: '2022-08-30 16:00:00',
            time: '2023-10-30T04:57:52.313Z',
          },
          {
            dt: 1661882400,
            main: {
              temp: 294.94,
              feels_like: 294.74,
              temp_min: 292.84,
              temp_max: 294.94,
              pressure: 1015,
              sea_level: 1015,
              grnd_level: 931,
              humidity: 60,
              temp_kf: 2.1,
            },
            weather: [
              {
                id: 500,
                main: 'Rain',
                description: 'light rain',
                icon: '10n',
              },
            ],
            clouds: {
              all: 93,
            },
            wind: {
              speed: 1.97,
              deg: 157,
              gust: 3.39,
            },
            visibility: 10000,
            pop: 0.33,
            rain: {
              '1h': 0.2,
            },
            sys: {
              pod: 'n',
            },
            dt_txt: '2022-08-30 18:00:00',
            time: '2023-10-30T05:57:52.313Z',
          },
          {
            dt: 1661878800,
            main: {
              temp: 296.31,
              feels_like: 296.07,
              temp_min: 296.2,
              temp_max: 296.31,
              pressure: 1015,
              sea_level: 1015,
              grnd_level: 932,
              humidity: 53,
              temp_kf: 0.11,
            },
            weather: [
              {
                id: 500,
                main: 'Rain',
                description: 'light rain',
                icon: '10d',
              },
            ],
            clouds: {
              all: 95,
            },
            wind: {
              speed: 1.58,
              deg: 103,
              gust: 3.52,
            },
            visibility: 10000,
            pop: 0.4,
            rain: {
              '1h': 0.24,
            },
            sys: {
              pod: 'd',
            },
            dt_txt: '2022-08-30 17:00:00',
            time: '2023-10-30T06:57:52.313Z',
          },
          {
            dt: 1662217200,
            main: {
              temp: 294.14,
              feels_like: 293.99,
              temp_min: 294.14,
              temp_max: 294.14,
              pressure: 1014,
              sea_level: 1014,
              grnd_level: 931,
              humidity: 65,
              temp_kf: 0,
            },
            weather: [
              {
                id: 804,
                main: 'Clouds',
                description: 'overcast clouds',
                icon: '04d',
              },
            ],
            clouds: {
              all: 100,
            },
            wind: {
              speed: 0.91,
              deg: 104,
              gust: 1.92,
            },
            visibility: 10000,
            pop: 0.53,
            sys: {
              pod: 'd',
            },
            dt_txt: '2022-09-03 15:00:00',
            time: '2023-10-30T07:57:52.313Z',
          },
          {
            dt: 1661882400,
            main: {
              temp: 294.94,
              feels_like: 294.74,
              temp_min: 292.84,
              temp_max: 294.94,
              pressure: 1015,
              sea_level: 1015,
              grnd_level: 931,
              humidity: 60,
              temp_kf: 2.1,
            },
            weather: [
              {
                id: 500,
                main: 'Rain',
                description: 'light rain',
                icon: '10n',
              },
            ],
            clouds: {
              all: 93,
            },
            wind: {
              speed: 1.97,
              deg: 157,
              gust: 3.39,
            },
            visibility: 10000,
            pop: 0.33,
            rain: {
              '1h': 0.2,
            },
            sys: {
              pod: 'n',
            },
            dt_txt: '2022-08-30 18:00:00',
            time: '2023-10-30T08:57:52.313Z',
          },
          {
            dt: 1661875200,
            main: {
              temp: 296.34,
              feels_like: 296.02,
              temp_min: 296.34,
              temp_max: 298.24,
              pressure: 1015,
              sea_level: 1015,
              grnd_level: 933,
              humidity: 50,
              temp_kf: -1.9,
            },
            weather: [
              {
                id: 500,
                main: 'Rain',
                description: 'light rain',
                icon: '10d',
              },
            ],
            clouds: {
              all: 97,
            },
            wind: {
              speed: 1.06,
              deg: 66,
              gust: 2.16,
            },
            visibility: 10000,
            pop: 0.32,
            rain: {
              '1h': 0.13,
            },
            sys: {
              pod: 'd',
            },
            dt_txt: '2022-08-30 16:00:00',
            time: '2023-10-30T09:57:52.313Z',
          },
          {
            dt: 1662217200,
            main: {
              temp: 294.14,
              feels_like: 293.99,
              temp_min: 294.14,
              temp_max: 294.14,
              pressure: 1014,
              sea_level: 1014,
              grnd_level: 931,
              humidity: 65,
              temp_kf: 0,
            },
            weather: [
              {
                id: 804,
                main: 'Clouds',
                description: 'overcast clouds',
                icon: '04d',
              },
            ],
            clouds: {
              all: 100,
            },
            wind: {
              speed: 0.91,
              deg: 104,
              gust: 1.92,
            },
            visibility: 10000,
            pop: 0.53,
            sys: {
              pod: 'd',
            },
            dt_txt: '2022-09-03 15:00:00',
            time: '2023-10-30T10:57:52.313Z',
          },
          {
            dt: 1661878800,
            main: {
              temp: 296.31,
              feels_like: 296.07,
              temp_min: 296.2,
              temp_max: 296.31,
              pressure: 1015,
              sea_level: 1015,
              grnd_level: 932,
              humidity: 53,
              temp_kf: 0.11,
            },
            weather: [
              {
                id: 500,
                main: 'Rain',
                description: 'light rain',
                icon: '10d',
              },
            ],
            clouds: {
              all: 95,
            },
            wind: {
              speed: 1.58,
              deg: 103,
              gust: 3.52,
            },
            visibility: 10000,
            pop: 0.4,
            rain: {
              '1h': 0.24,
            },
            sys: {
              pod: 'd',
            },
            dt_txt: '2022-08-30 17:00:00',
            time: '2023-10-30T11:57:52.313Z',
          },
          {
            dt: 1662217200,
            main: {
              temp: 294.14,
              feels_like: 293.99,
              temp_min: 294.14,
              temp_max: 294.14,
              pressure: 1014,
              sea_level: 1014,
              grnd_level: 931,
              humidity: 65,
              temp_kf: 0,
            },
            weather: [
              {
                id: 804,
                main: 'Clouds',
                description: 'overcast clouds',
                icon: '04d',
              },
            ],
            clouds: {
              all: 100,
            },
            wind: {
              speed: 0.91,
              deg: 104,
              gust: 1.92,
            },
            visibility: 10000,
            pop: 0.53,
            sys: {
              pod: 'd',
            },
            dt_txt: '2022-09-03 15:00:00',
            time: '2023-10-30T12:57:52.313Z',
          },
        ],
        city: {
          id: 3163858,
          name: 'Zocca',
          coord: {
            lat: 44.34,
            lon: 10.99,
          },
          country: 'IT',
          population: 4593,
          timezone: 7200,
          sunrise: 1661834187,
          sunset: 1661882248,
        },
      }),
    );
  },
);

const mockHandlers = [mockGetHourlyForecast];
export default mockHandlers;
