import type { Meta, StoryObj } from '@storybook/react';

import ChartOverview from '.';

export default {
  title: 'Common/ChartOverview',
  component: ChartOverview,
} as Meta<typeof ChartOverview>;
type Story = StoryObj<typeof ChartOverview>;

export const Basic: Story = {
  args: {
    coords: [52.52, 13.405],
  },
};
