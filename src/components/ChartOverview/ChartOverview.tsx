import useWeatherSocket from '@/hooks/useWeatherSocket';
import { memo, useEffect, useState } from 'react';
import { ToggleGroup, ToggleGroupItem } from '@/ui/toggleGroup';
import { ValuesOf } from '@/types/helpers';
import {
  options,
  celsiusYAxis,
  fahrenheitYAxis,
  uvYAxis,
  precipitationYAxis,
  windfallYAxis,
} from './charts';
import { CoordinateTuple, RealtimeWeather } from '@/types/weather';
import { TemperatureUnit, temperatureUnits } from '@/constants/weather';
import {
  kelvinToCelsius,
  kelvinToFahrenheit,
  kelvinToUnitStr,
} from '@/utils/display';
import { useTemperatureUnit } from '@/contexts/tempUnitContext';
import ReactApexChart from 'react-apexcharts';

const chartTypes = {
  CELSIUS: 'CELSIUS',
  FAHRENHEIT: 'FAHRENHEIT',
  UVINDEX: 'UVINDEX',
  PRECIPITATION: 'PRECIPITATION',
  WINDSPEED: 'WINDSPEED',
} as const;
type ChartType = ValuesOf<typeof chartTypes>;

const chartOptionsMap = {
  [chartTypes.CELSIUS]: celsiusYAxis,
  [chartTypes.FAHRENHEIT]: fahrenheitYAxis,
  [chartTypes.UVINDEX]: uvYAxis,
  [chartTypes.PRECIPITATION]: precipitationYAxis,
  [chartTypes.WINDSPEED]: windfallYAxis,
};

const chartTypeToDataField: Record<ChartType, keyof RealtimeWeather> = {
  [chartTypes.CELSIUS]: 'temperature',
  [chartTypes.FAHRENHEIT]: 'temperature',
  [chartTypes.UVINDEX]: 'uvIndex',
  [chartTypes.PRECIPITATION]: 'precipitation',
  [chartTypes.WINDSPEED]: 'windSpeed',
};

const getDataSeries = (
  data: RealtimeWeather[],
  field: keyof RealtimeWeather,
  unit: TemperatureUnit,
) => {
  if (field === 'temperature') {
    const transform =
      unit === temperatureUnits.FAHRENHEIT
        ? kelvinToFahrenheit
        : kelvinToCelsius;
    return [{ data: data.map((values) => transform(values.temperature)) }];
  }
  return [{ data: data.map((values) => values[field]) }];
};

const Chart = memo(function Chart() {
  return (
    <ReactApexChart
      options={options}
      series={[{ data: [] }]}
      type="line"
      height={350}
    />
  );
});

type ChartOverviewProps = {
  coords: CoordinateTuple;
};
const ChartOverview = ({ coords }: ChartOverviewProps) => {
  const { unit } = useTemperatureUnit();
  const temperatureChartType =
    unit === temperatureUnits.FAHRENHEIT
      ? chartTypes.FAHRENHEIT
      : chartTypes.CELSIUS;
  const [type, setType] = useState<ChartType>(temperatureChartType);
  const { data, onStart } = useWeatherSocket({ coords });

  useEffect(() => {
    onStart();
  }, [onStart]);

  useEffect(() => {
    if (!data) return;
    const series = getDataSeries(
      data.realtime,
      chartTypeToDataField[type],
      unit,
    );
    ApexCharts.exec('realtime', 'updateSeries', series);
  }, [data, type, unit]);

  // sync when unit changes but chart is showing a temperature
  useEffect(() => {
    if (type === temperatureChartType) return;
    if (type !== chartTypes.CELSIUS && type !== chartTypes.FAHRENHEIT) return;
    setType(temperatureChartType);
    ApexCharts.exec(
      'realtime',
      'updateOptions',
      { yaxis: chartOptionsMap[type] },
      false,
      false,
    );
  }, [temperatureChartType, type]);

  return (
    <div className="w-full rounded-2xl bg-gray-100 p-4">
      <div className="flex justify-between">
        <div className="text-lg font-medium">Overview</div>
        <ToggleGroup
          type="single"
          value={type}
          onValueChange={(value: ChartType) => {
            if (!value) return;
            setType(value);
            ApexCharts.exec(
              'realtime',
              'updateOptions',
              { yaxis: chartOptionsMap[value] },
              false,
              false,
            );
          }}
          aria-label="Chart type"
        >
          <ToggleGroupItem value={temperatureChartType}>
            Temperature
          </ToggleGroupItem>
          <ToggleGroupItem value={chartTypes.UVINDEX}>UV Index</ToggleGroupItem>
          <ToggleGroupItem value={chartTypes.PRECIPITATION}>
            Rainfall
          </ToggleGroupItem>
          <ToggleGroupItem value={chartTypes.WINDSPEED}>
            Wind Speed
          </ToggleGroupItem>
        </ToggleGroup>
      </div>
      <Chart />
    </div>
  );
};

export default ChartOverview;
