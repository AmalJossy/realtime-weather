import { ApexOptions } from 'apexcharts';
import { memo } from 'react';
import ReactApexChart from 'react-apexcharts';

export const options: ApexOptions = {
  chart: {
    id: 'realtime',
    height: 350,
    width: 800,
    type: 'line',
    animations: {
      enabled: true,
      easing: 'linear',
      dynamicAnimation: {
        speed: 1000,
      },
    },
    toolbar: {
      show: false,
    },
    zoom: {
      enabled: false,
    },
  },
  dataLabels: {
    enabled: false,
  },
  stroke: {
    curve: 'smooth',
  },
  markers: {
    size: 0,
  },
  xaxis: {
    type: 'category',
    categories: [
      '-50s',
      '-40s',
      '-30s',
      '-20s',
      '-10s',
      '0s',
      '+10s',
      '+20s',
      '+30s',
      '+40s',
      '+50s',
      '+60s',
    ],
  },
  legend: {
    show: false,
  },
  yaxis: {
    max: 40,
    min: -10,
    decimalsInFloat: 0,
  },
};
export const celsiusYAxis = {
  max: 40,
  min: -10,
  decimalsInFloat: 0,
};
export const fahrenheitYAxis = {
  max: 100,
  min: 20,
  decimalsInFloat: 0,
};
export const uvYAxis = {
  max: 10,
  min: 0,
  decimalsInFloat: 0,
};
export const precipitationYAxis = {
  max: 20,
  min: 0,
  decimalsInFloat: 0,
};
export const windfallYAxis = {
  max: 15,
  min: 0,
  decimalsInFloat: 0,
};
