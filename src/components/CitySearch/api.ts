import apiMap from '@/constants/apiMap';
import { GeoSearchResponse } from '@/types/apiResponses/geoSearch';

export const getCities = async (searchStr = '') => {
  const response = await fetch(`${apiMap.GEO_SEARCH}?q=${searchStr}`);
  const data: GeoSearchResponse = await response.json();
  return data.results;
};
