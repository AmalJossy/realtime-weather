import { CityGeoData } from '@/types/apiResponses/geoSearch';
import {
  CommandDialog,
  CommandEmpty,
  CommandGroup,
  CommandInput,
  CommandItem,
  CommandList,
} from '@/ui/command';
import { Input } from '@/ui/input';
import { useEffect, useState } from 'react';
import { getCities } from './api';
import { CommandLoading } from 'cmdk';

type CitySearchProps = {
  onSelect: (city: CityGeoData) => void;
};
const CitySearch = ({ onSelect }: CitySearchProps) => {
  const [open, setOpen] = useState(false);
  const [isLoading, setIsLoading] = useState(true);
  const [cities, setCities] = useState<CityGeoData[]>([]);

  useEffect(() => {
    const fetchInitial = async () => {
      const initialCities = await getCities();
      setCities(initialCities);
      setIsLoading(false);
    };
    fetchInitial();
  }, []);

  return (
    <>
      <Input onClick={() => setOpen(true)} placeholder="find city" />
      <CommandDialog open={open} onOpenChange={setOpen}>
        <CommandInput placeholder="search..." />
        <CommandList>
          <CommandEmpty>No results found.</CommandEmpty>
          <CommandGroup heading="Suggestions">
            {isLoading && <CommandLoading>Finding cities…</CommandLoading>}
            {cities.map((city) => (
              <CommandItem
                key={`${city.name}:${city.country}`}
                onSelect={() => {
                  onSelect(city);
                  setOpen(false);
                }}
              >
                {city.name}, {city.country}
              </CommandItem>
            ))}
          </CommandGroup>
        </CommandList>
      </CommandDialog>
    </>
  );
};

export default CitySearch;
