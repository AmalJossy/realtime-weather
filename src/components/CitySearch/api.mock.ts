import apiMap from '@/constants/apiMap';
import { rest } from 'msw';

export const mockGetCities = rest.get(
  `*${apiMap.GEO_SEARCH}`,
  async (_, res, ctx) => {
    return res(
      ctx.status(200),
      ctx.json({
        results: [
          {
            name: 'New York City',
            country: 'USA',
            coordinates: [40.7128, -74.006],
          },
          {
            name: 'Los Angeles',
            country: 'USA',
            coordinates: [34.0522, -118.2437],
          },
          {
            name: 'London',
            country: 'UK',
            coordinates: [51.5074, -0.1278],
          },
          {
            name: 'Paris',
            country: 'France',
            coordinates: [48.8566, 2.3522],
          },
          {
            name: 'Tokyo',
            country: 'Japan',
            coordinates: [35.682839, 139.759455],
          },
          {
            name: 'Sydney',
            country: 'Australia',
            coordinates: [-33.865143, 151.2099],
          },
          {
            name: 'Rio de Janeiro',
            country: 'Brazil',
            coordinates: [-22.9068, -43.1729],
          },
          {
            name: 'Cape Town',
            country: 'South Africa',
            coordinates: [-33.9249, 18.4241],
          },
          {
            name: 'Rome',
            country: 'Italy',
            coordinates: [41.9028, 12.4964],
          },
          {
            name: 'Toronto',
            country: 'Canada',
            coordinates: [43.65107, -79.347015],
          },
        ],
      }),
    );
  },
);

const mockHandlers = [mockGetCities];
export default mockHandlers;
