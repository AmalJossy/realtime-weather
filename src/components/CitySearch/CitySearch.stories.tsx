import type { Meta, StoryObj } from '@storybook/react';
import mockHandlers from './api.mock';

import CitySearch from '.';

export default {
  title: 'Common/CitySearch',
  component: CitySearch,
  parameters: {
    msw: mockHandlers,
  },
} as Meta<typeof CitySearch>;
type Story = StoryObj<typeof CitySearch>;

export const Basic: Story = {};
