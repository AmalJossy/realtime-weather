import type { Meta, StoryObj } from '@storybook/react';

import MapPreview from '.';

export default {
  title: 'Common/MapPreview',
  component: MapPreview,
  parameters: {
    layout: 'centered',
  },
} as Meta<typeof MapPreview>;
type Story = StoryObj<typeof MapPreview>;

export const Basic: Story = {
  args: {
    coords: [52.52, 13.405],
  },
};
