import { MapContainer } from 'react-leaflet/MapContainer';
import { TileLayer } from 'react-leaflet/TileLayer';
import { Marker } from 'react-leaflet/Marker';
import { Popup } from 'react-leaflet/Popup';
import { useEffect, useState } from 'react';
import { Map } from 'leaflet';
import { CoordinateTuple } from '@/types/weather';

import 'leaflet/dist/leaflet.css';

type MapPreviewProps = { coords: CoordinateTuple };

// const attribution = `&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributor`
// const tileUrl = `https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png`

const attribution = `&copy; <a href="https://www.stadiamaps.com/" target="_blank">Stadia Maps</a> &copy; <a href="https://openmaptiles.org/" target="_blank">OpenMapTiles</a> &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors`;
const tileUrl = `https://tiles.stadiamaps.com/tiles/alidade_smooth/{z}/{x}/{y}{r}.png`;
const temperatureTiles = `https://tile.openweathermap.org/map/temp_new/{z}/{x}/{y}.png?appid=06aac0fd4ba239a20d824ef89602f311`;

const MapPreview = ({ coords }: MapPreviewProps) => {
  const [map, setMap] = useState<Map | null>(null);

  const [lat, lng] = coords;
  useEffect(() => {
    if (!map) return;
    map.setView([lat, lng], 13);
  }, [lat, lng, map]);

  return (
    <MapContainer
      center={coords}
      zoom={13}
      scrollWheelZoom={false}
      className="h-64 w-full rounded-lg lg:w-96"
      ref={setMap}
    >
      <TileLayer attribution={attribution} url={tileUrl} />
      <TileLayer url={temperatureTiles} />
    </MapContainer>
  );
};
export default MapPreview;
