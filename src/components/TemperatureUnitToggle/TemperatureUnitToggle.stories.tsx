import type { Meta, StoryObj } from '@storybook/react';

import TemperatureUnitToggle from '.';

export default {
  title: 'Common/TemperatureUnitToggle',
  component: TemperatureUnitToggle,
} as Meta<typeof TemperatureUnitToggle>;
type Story = StoryObj<typeof TemperatureUnitToggle>;

export const Basic: Story = {};
