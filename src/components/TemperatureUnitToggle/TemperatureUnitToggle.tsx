import { TemperatureUnit, temperatureUnits } from '@/constants/weather';
import { useTemperatureUnit } from '@/contexts/tempUnitContext';
import { ToggleGroup, ToggleGroupItem } from '@/ui/toggleGroup';

const TemperatureUnitToggle = () => {
  const { unit, setUnit } = useTemperatureUnit();
  return (
    <ToggleGroup
      type="single"
      value={unit}
      onValueChange={(value: TemperatureUnit) => {
        if (value) setUnit(value);
      }}
      aria-label="Temperature unit"
    >
      <ToggleGroupItem value={temperatureUnits.CELSIUS}>°C</ToggleGroupItem>
      <ToggleGroupItem value={temperatureUnits.FAHRENHEIT}>°F</ToggleGroupItem>
    </ToggleGroup>
  );
};
export default TemperatureUnitToggle;
