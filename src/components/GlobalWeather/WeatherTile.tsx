import { useTemperatureUnit } from '@/contexts/tempUnitContext';
import { kelvinToUnitStr } from '@/utils/display';

type WeatherTileProps = {
  weather: string;
  cityName: string;
  country: string;
  icon: string;
  max: number;
  min: number;
};
const WeatherTile = ({
  icon,
  max,
  min,
  cityName,
  country,
  weather,
}: WeatherTileProps) => {
  const { unit } = useTemperatureUnit();
  const minTemp = kelvinToUnitStr(min, unit);
  const maxTemp = kelvinToUnitStr(max, unit);
  return (
    <div className="relative my-2 ml-8 rounded-xl bg-gray-200 p-4 pl-12">
      <img
        src={`http://openweathermap.org/img/wn/${icon}@2x.png`}
        alt={weather}
        className="absolute left-0 h-16 w-16 -translate-x-8 rounded-full  bg-white"
      />
      <div className="flex items-center justify-between">
        <div>
          <div className="font-medium">{cityName}</div>
          <div className="text-sm font-light">{country}</div>
        </div>
        <div className="text-lg">
          {maxTemp}/<span className="text-sm">{minTemp}</span>
        </div>
      </div>
    </div>
  );
};

export default WeatherTile;
