import type { Meta, StoryObj } from '@storybook/react';
import mockHandlers from './api.mock';

import GlobalWeather from '.';

export default {
  title: 'Common/GlobalWeather',
  component: GlobalWeather,
  parameters: {
    msw: mockHandlers,
  },
} as Meta<typeof GlobalWeather>;
type Story = StoryObj<typeof GlobalWeather>;

export const Basic: Story = {};
