import { useEffect, useState } from 'react';
import { getGlobalWeather } from './api';
import { CurrentWeather } from '@/types/apiResponses/currentWeather';
import WeatherTile from './WeatherTile';

type GlobalWeatherProps = Record<string, never>;

const GlobalWeather = ({}: GlobalWeatherProps) => {
  const [globalWeather, setGlobalWeather] = useState<CurrentWeather[]>();
  useEffect(() => {
    const initialFetch = async () => {
      const data = await getGlobalWeather();
      setGlobalWeather(data);
    };
    initialFetch();
  }, []);
  if (!globalWeather) return null;
  return (
    <div className="rounded-2xl bg-gray-100 p-4">
      <div className="h-96 overflow-auto">
        {globalWeather.map((weather) => (
          <WeatherTile
            key={weather.id}
            icon={weather.weather[0].icon}
            cityName={weather.city}
            country={weather.country}
            max={weather.main.temp_max}
            min={weather.main.temp_min}
            weather={weather.weather[0].main}
          />
        ))}
      </div>
    </div>
  );
};
export default GlobalWeather;
