import apiMap from '@/constants/apiMap';
import { rest } from 'msw';

export const mockGetGlobalWeather = rest.get(
  `*${apiMap.CURRENT_WEATHER_POPULAR}`,
  async (_, res, ctx) => {
    return res(
      ctx.status(200),
      ctx.json([
        {
          coord: {
            lon: -74.006,
            lat: 40.7128,
          },
          weather: [
            {
              id: 803,
              main: 'Clouds',
              description: 'broken clouds',
              icon: '04d',
            },
          ],
          base: 'stations',
          main: {
            temp: 285.69,
            feels_like: 285.04,
            temp_min: 283.23,
            temp_max: 287.58,
            pressure: 1020,
            humidity: 78,
          },
          visibility: 10000,
          wind: {
            speed: 5.36,
            deg: 46,
            gust: 7.6,
          },
          clouds: {
            all: 75,
          },
          dt: 1698587869,
          sys: {
            type: 2,
            id: 2008101,
            country: 'US',
            sunrise: 1698578546,
            sunset: 1698616613,
          },
          timezone: -14400,
          id: 5128581,
          name: 'New York',
          cod: 200,
          city: 'New York City',
          country: 'USA',
        },
        {
          coord: {
            lon: -118.2437,
            lat: 34.0522,
          },
          weather: [
            {
              id: 800,
              main: 'Clear',
              description: 'clear sky',
              icon: '01n',
            },
          ],
          base: 'stations',
          main: {
            temp: 288.37,
            feels_like: 287.57,
            temp_min: 285.85,
            temp_max: 292.25,
            pressure: 1024,
            humidity: 62,
          },
          visibility: 10000,
          wind: {
            speed: 1.79,
            deg: 63,
            gust: 4.92,
          },
          clouds: {
            all: 0,
          },
          dt: 1698587724,
          sys: {
            type: 1,
            id: 3694,
            country: 'US',
            sunrise: 1698588579,
            sunset: 1698627813,
          },
          timezone: -25200,
          id: 5368361,
          name: 'Los Angeles',
          cod: 200,
          city: 'Los Angeles',
          country: 'USA',
        },
        {
          coord: {
            lon: -0.1277,
            lat: 51.5073,
          },
          weather: [
            {
              id: 500,
              main: 'Rain',
              description: 'light rain',
              icon: '10d',
            },
          ],
          base: 'stations',
          main: {
            temp: 283.94,
            feels_like: 283.5,
            temp_min: 283.03,
            temp_max: 284.84,
            pressure: 989,
            humidity: 93,
          },
          visibility: 7000,
          wind: {
            speed: 2.57,
            deg: 240,
          },
          rain: {
            '1h': 0.75,
          },
          clouds: {
            all: 40,
          },
          dt: 1698587384,
          sys: {
            type: 2,
            id: 2006068,
            country: 'GB',
            sunrise: 1698562071,
            sunset: 1698597627,
          },
          timezone: 0,
          id: 2643743,
          name: 'London',
          cod: 200,
          city: 'London',
          country: 'UK',
        },
        {
          coord: {
            lon: 2.3522,
            lat: 48.8566,
          },
          weather: [
            {
              id: 803,
              main: 'Clouds',
              description: 'broken clouds',
              icon: '04d',
            },
          ],
          base: 'stations',
          main: {
            temp: 289.4,
            feels_like: 288.83,
            temp_min: 288.58,
            temp_max: 290.38,
            pressure: 995,
            humidity: 67,
          },
          visibility: 10000,
          wind: {
            speed: 9.26,
            deg: 220,
          },
          clouds: {
            all: 75,
          },
          dt: 1698587869,
          sys: {
            type: 2,
            id: 2041230,
            country: 'FR',
            sunrise: 1698561112,
            sunset: 1698597396,
          },
          timezone: 3600,
          id: 6455259,
          name: 'Paris',
          cod: 200,
          city: 'Paris',
          country: 'France',
        },
        {
          coord: {
            lon: 139.7577,
            lat: 35.6813,
          },
          weather: [
            {
              id: 801,
              main: 'Clouds',
              description: 'few clouds',
              icon: '02n',
            },
          ],
          base: 'stations',
          main: {
            temp: 289.57,
            feels_like: 288.57,
            temp_min: 285.35,
            temp_max: 291.46,
            pressure: 1015,
            humidity: 50,
          },
          visibility: 10000,
          wind: {
            speed: 7.2,
            deg: 330,
          },
          clouds: {
            all: 20,
          },
          dt: 1698587386,
          sys: {
            type: 2,
            id: 268395,
            country: 'JP',
            sunrise: 1698526753,
            sunset: 1698565803,
          },
          timezone: 32400,
          id: 1861060,
          name: 'Japan',
          cod: 200,
          city: 'Tokyo',
          country: 'Japan',
        },
      ]),
    );
  },
);

const mockHandlers = [mockGetGlobalWeather];
export default mockHandlers;
