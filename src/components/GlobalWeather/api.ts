import apiMap from '@/constants/apiMap';
import { CurrentWeather } from '@/types/apiResponses/currentWeather';

export const getGlobalWeather = async (searchStr = '') => {
  const response = await fetch(
    `${apiMap.CURRENT_WEATHER_POPULAR}?q=${searchStr}`,
  );
  const data: CurrentWeather[] = await response.json();
  return data;
};
