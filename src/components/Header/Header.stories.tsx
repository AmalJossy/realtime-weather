import type { Meta, StoryObj } from "@storybook/react";

import Header from ".";

export default {
  title: "Common/Header",
  component: Header,
} as Meta<typeof Header>;
type Story = StoryObj<typeof Header>;

export const Basic: Story = {
  args: {
    children: "Mon, 15 May, 2023",
  },
};
