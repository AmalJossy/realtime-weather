import { ReactNode } from 'react';

type HeaderProps = {
  avatar?: ReactNode;
  children: ReactNode;
  actions: ReactNode;
};

const Header = ({ actions, avatar, children }: HeaderProps) => {
  return (
    <div className="mb-4 mt-8 flex">
      <div className="">{avatar}</div>
      <div className="mx-4 flex flex-1 items-center text-xl font-medium">
        {children}
      </div>
      <div className="flex gap-4">{actions}</div>
    </div>
  );
};
export default Header;
