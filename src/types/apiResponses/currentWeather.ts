import { WeatherCondition, WeatherDescription, Wind } from '../weather';

export type CurrentWeather = {
  weather: [WeatherDescription];
  main: WeatherCondition;
  wind: Wind;
  id: string;
  city: string;
  country: string;
};
