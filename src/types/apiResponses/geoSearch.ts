import { CoordinateTuple } from '../weather';

export type CityGeoData = {
  name: string;
  country: string;
  coordinates: CoordinateTuple;
};

export type GeoSearchResponse = {
  results: CityGeoData[];
};
