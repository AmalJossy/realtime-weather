import { WeatherForecast } from '../weather';

export type ForecastCityContext = {
  id: number;
  name: string;
  coord: {
    lat: number;
    lon: number;
  };
  country: string;
  population: number;
  timezone: number;
  sunrise: number;
  sunset: number;
};

export type WeatherForecastResponse = {
  list: WeatherForecast[];
  city: ForecastCityContext;
};
