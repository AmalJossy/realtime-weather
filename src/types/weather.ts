export type RealtimeWeather = {
  temperature: number;
  precipitation: number;
  windSpeed: number;
  uvIndex: number;
};

export type CoordinateTuple = [number, number];

export type WeatherData = {
  realtime: RealtimeWeather[];
};

export type WeatherCondition = {
  temp: number;
  feels_like: number;
  temp_min: number;
  temp_max: number;
  pressure: number;
  sea_level: number;
  grnd_level: number;
  humidity: number;
  temp_kf: number;
};

export type WeatherDescription = {
  id: number;
  main: string;
  description: string;
  icon: string;
};

export type Wind = {
  speed: number;
  deg: number;
  gust: number;
};

export type WeatherForecast = {
  main: WeatherCondition;
  weather: [WeatherDescription];
  time: string;
  wind: Wind;
};
