import { wsEvents } from '@/constants/socketEvents';
import { useSocket } from '@/contexts/socketContext';
import { ValuesOf } from '@/types/helpers';
import { CoordinateTuple, WeatherData } from '@/types/weather';
import { useState, useEffect } from 'react';

const socketStatus = {
  INACTIVE: 'INACTIVE',
  ACTIVE: 'ACTIVE',
} as const;
type SocketStatus = ValuesOf<typeof socketStatus>;

type WeatherSocketArgs = {
  coords: CoordinateTuple;
};
const useWeatherSocket = ({ coords }: WeatherSocketArgs) => {
  const [data, setData] = useState<WeatherData>();
  const [status, setStatus] = useState<SocketStatus>(socketStatus.INACTIVE);

  const socket = useSocket();
  useEffect(() => {
    const weatherListener = (msg: WeatherData) => {
      setData(msg);
    };
    if (!socket) return;
    socket.on(wsEvents.WEATHER_DATA, weatherListener);
    return () => {
      socket.removeListener(wsEvents.WEATHER_DATA, weatherListener);
      setStatus(socketStatus.INACTIVE);
    };
  }, [socket]);

  useEffect(() => {
    if (!socket || status === socketStatus.INACTIVE) return;
    socket.emit(wsEvents.WEATHER_START, { lat: coords[0], lon: coords[1] });
  }, [coords, socket, status]);

  const onStart = () => {
    if (!socket || status !== socketStatus.INACTIVE) return;
    socket.emit(wsEvents.WEATHER_START, { lat: coords[0], lon: coords[1] });
    setStatus(socketStatus.ACTIVE);
  };

  return {
    data,
    status,
    onStart,
  };
};

export default useWeatherSocket;
