import {
  ReactNode,
  createContext,
  useContext,
  useEffect,
  useState,
} from 'react';
import { Socket, io } from 'socket.io-client';

const SocketContext = createContext<Socket | undefined>(undefined);

type SocketProviderProps = {
  children: ReactNode;
};

export const useSocket = () => {
  return useContext(SocketContext);
};

const SocketProvider = ({ children }: SocketProviderProps) => {
  const [socket, setSocket] = useState<Socket>();
  useEffect(() => {
    if (socket) {
      socket.connect();
      console.log('connecting');
      return () => {
        socket.disconnect();
        console.log('disconnecting');
      };
    }
    const setupSocket = async () => {
      await fetch('/api/io');
      const newSocket = io({
        reconnection: true,
        reconnectionDelay: 1000,
        reconnectionAttempts: 10,
      });
      setSocket(newSocket);
    };
    setupSocket();
  });
  return (
    <SocketContext.Provider value={socket}>{children}</SocketContext.Provider>
  );
};

export default SocketProvider;
