import { TemperatureUnit, temperatureUnits } from '@/constants/weather';
import { ReactNode, createContext, useContext, useState } from 'react';

const TemperatureUnitContext = createContext<{
  unit: TemperatureUnit;
  setUnit: (unit: TemperatureUnit) => void;
}>({
  unit: temperatureUnits.CELSIUS,
  setUnit: () => null,
});

type TemperatureUnitProviderProps = {
  children: ReactNode;
};

export const useTemperatureUnit = () => {
  return useContext(TemperatureUnitContext);
};

const TemperatureUnitProvider = ({
  children,
}: TemperatureUnitProviderProps) => {
  const [unit, setUnit] = useState<TemperatureUnit>(temperatureUnits.CELSIUS);
  return (
    <TemperatureUnitContext.Provider value={{ unit, setUnit }}>
      {children}
    </TemperatureUnitContext.Provider>
  );
};

export default TemperatureUnitProvider;
