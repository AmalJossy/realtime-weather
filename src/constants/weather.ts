import { ValuesOf } from '@/types/helpers';

export const temperatureUnits = {
  CELSIUS: 'celsius',
  FAHRENHEIT: 'fahrenheit',
} as const;
export type TemperatureUnit = ValuesOf<typeof temperatureUnits>;
