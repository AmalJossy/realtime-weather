export const wsEvents = {
  WEATHER_START: 'weather:start',
  WEATHER_DATA: 'weather:data',
  WEATHER_STOP: 'weather:stop',
};
