const apiMap = {
  HOURLY_FORECAST: `/api/forecast/hourly`,
  GEO_SEARCH: `/api/geo/search`,
  CURRENT_WEATHER_POPULAR: `/api/current/popular`,
};

export default apiMap;
