import type { Meta, StoryObj } from '@storybook/react';
import citySearchMocks from '@/components/CitySearch/api.mock';
import globalWeatherMocks from '@/components/GlobalWeather/api.mock';
import hourlyForecastMocks from '@/components/HourlyForecast/api.mock';

import Dashboard from '.';

export default {
  title: 'Dashboard',
  component: Dashboard,
  parameters: {
    msw: [...citySearchMocks, ...globalWeatherMocks, ...hourlyForecastMocks],
  },
} as Meta<typeof Dashboard>;
type Story = StoryObj<typeof Dashboard>;

export const Basic: Story = {
  render: () => <Dashboard />,
};
