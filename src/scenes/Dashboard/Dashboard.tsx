import CitySearch from '@/components/CitySearch';
import GlobalWeather from '@/components/GlobalWeather';
import Header from '@/components/Header';
import HourlyForecast from '@/components/HourlyForecast';
import TemperatureUnitToggle from '@/components/TemperatureUnitToggle';
import TemperatureUnitProvider from '@/contexts/tempUnitContext';
import { CityGeoData } from '@/types/apiResponses/geoSearch';
import dynamic from 'next/dynamic';
import { useState } from 'react';

const ChartOverview = dynamic(() => import('@/components/ChartOverview'), {
  ssr: false,
});

const MapPreview = dynamic(() => import('@/components/MapPreview'), {
  ssr: false,
});

const Dashboard = () => {
  const [city, setCity] = useState<CityGeoData>({
    name: 'Berlin',
    country: 'Germany',
    coordinates: [52.52, 13.405],
  });
  return (
    <TemperatureUnitProvider>
      <div className="container">
        <Header
          actions={
            <>
              <CitySearch onSelect={(city) => setCity(city)} />
              <TemperatureUnitToggle />
            </>
          }
        >
          {null}
        </Header>
        <div className="flex flex-col justify-around gap-4 lg:flex-row">
          <div className="w-full lg:w-4/6">
            <div className="my-4">
              <HourlyForecast city={city} />
            </div>
            <div className="my-4">
              <ChartOverview coords={city.coordinates} />
            </div>
          </div>
          <div>
            <div>
              <div className="my-4">
                <MapPreview coords={city.coordinates} />
              </div>
              <div className="my-4">
                <GlobalWeather />
              </div>
            </div>
          </div>
        </div>
      </div>
    </TemperatureUnitProvider>
  );
};
export default Dashboard;
