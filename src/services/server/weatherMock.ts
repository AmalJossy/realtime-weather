import { RealtimeWeather, WeatherData } from '@/types/weather';

type MaxRealtimeWeather = {
  maxTemperature: number;
  maxPrecipitation: number;
  maxWindSpeed: number;
  maxUVIndex: number;
};

const nightTime = new Date('2023-01-01T00:00:00');
const morningTime = new Date('2023-01-01T06:00:00');
const noonTime = new Date('2023-01-01T12:00:00');
const eveningTime = new Date('2023-01-01T18:00:00');
const nextNightTime = new Date('2023-01-01T24:00:00');

const generateMockWeatherData = (
  currentTime: Date,
  lastData: RealtimeWeather,
  interval: number,
  maxValues: MaxRealtimeWeather,
) => {
  // extract the maximum values for each parameter
  const { maxPrecipitation, maxWindSpeed, maxUVIndex } = maxValues;
  const maxTemperature = maxValues.maxTemperature - 273;

  // Define the patterns for daily variations
  const temperaturePattern = [
    { time: nightTime, value: 0 }, // Midnight: Lowest temperature
    { time: morningTime, value: maxTemperature / 3 }, // Morning: Increase
    { time: noonTime, value: maxTemperature }, // Noon: Highest temperature
    { time: eveningTime, value: maxTemperature / 3 }, // Evening: Decrease
    { time: nextNightTime, value: 0 }, // Midnight: Lowest temperature
  ];

  const precipitationPattern = [
    { time: nightTime, value: 0 }, // Midnight: No precipitation
    { time: morningTime, value: maxPrecipitation / 3 }, // Morning: Increase
    { time: noonTime, value: maxPrecipitation }, // Noon: Highest precipitation
    { time: eveningTime, value: maxPrecipitation / 3 }, // Evening: Decrease
    { time: nextNightTime, value: 0 }, // Midnight: No precipitation
  ];

  const windSpeedPattern = [
    { time: nightTime, value: 0 }, // Midnight: Calm winds
    { time: morningTime, value: maxWindSpeed / 3 }, // Morning: Increase
    { time: noonTime, value: maxWindSpeed }, // Noon: Highest wind speed
    { time: eveningTime, value: maxWindSpeed / 3 }, // Evening: Decrease
    { time: nextNightTime, value: 0 }, // Midnight: Calm winds
  ];

  const uvIndexPattern = [
    { time: nightTime, value: 0 }, // Midnight: Low UV index
    { time: morningTime, value: maxUVIndex / 3 }, // Morning: Increase
    { time: noonTime, value: maxUVIndex }, // Noon: Highest UV index
    { time: eveningTime, value: maxUVIndex / 3 }, // Evening: Decrease
    { time: nextNightTime, value: 0 }, // Midnight: Low UV index
  ];

  // Find the current pattern segment based on the current time
  let segmentIndex = 0;
  for (let i = 0; i < temperaturePattern.length - 1; i++) {
    if (
      temperaturePattern[i].time <= currentTime &&
      temperaturePattern[i + 1].time > currentTime
    ) {
      segmentIndex = i;
      break;
    }
  }

  // Calculate the interpolated values based on the pattern segment and time elapsed
  const timeDiff = 6 * 3600000;
  const currentTimeDiff =
    currentTime.getTime() -
    new Date(temperaturePattern[segmentIndex].time).getTime();

  // Calculate the interpolation factor based on the time elapsed
  const interpolationFactor = (currentTimeDiff + interval) / timeDiff; // 86400 seconds in a day

  const temperatureDelta =
    temperaturePattern[segmentIndex + 1].value -
    temperaturePattern[segmentIndex].value;
  const temperatureOffset = Math.random() * 4 - 2;
  const temperatureInterpolated =
    (temperatureDelta / timeDiff) * interpolationFactor +
    temperaturePattern[segmentIndex].value +
    273 +
    temperatureOffset;

  const precipitationDelta =
    precipitationPattern[segmentIndex + 1].value -
    precipitationPattern[segmentIndex].value;
  const precipitationInterpolated =
    (precipitationDelta / timeDiff) * interpolationFactor +
    precipitationPattern[segmentIndex].value +
    Math.random();

  const windSpeedDelta =
    windSpeedPattern[segmentIndex + 1].value -
    windSpeedPattern[segmentIndex].value;
  const windSpeedInterpolated =
    (windSpeedDelta / timeDiff) * interpolationFactor +
    windSpeedPattern[segmentIndex].value +
    Math.random();

  const uvIndexDelta =
    uvIndexPattern[segmentIndex + 1].value - uvIndexPattern[segmentIndex].value;
  const uvIndexInterpolated =
    (uvIndexDelta / timeDiff) * interpolationFactor +
    uvIndexPattern[segmentIndex].value +
    Math.random();

  return {
    temperature: temperatureInterpolated,
    precipitation: precipitationInterpolated,
    windSpeed: windSpeedInterpolated,
    uvIndex: uvIndexInterpolated,
  };
};

const response = {
  coord: {
    lon: 10.99,
    lat: 44.34,
  },
  weather: [
    {
      id: 501,
      main: 'Rain',
      description: 'moderate rain',
      icon: '10d',
    },
  ],
  base: 'stations',
  main: {
    temp: 298.48,
    feels_like: 298.74,
    temp_min: 297.56,
    temp_max: 300.05,
    pressure: 1015,
    humidity: 64,
    sea_level: 1015,
    grnd_level: 933,
  },
  visibility: 10000,
  wind: {
    speed: 0.62,
    deg: 349,
    gust: 1.18,
  },
  rain: {
    '1h': 3.16,
  },
  clouds: {
    all: 100,
  },
  dt: 1661870592,
  sys: {
    type: 2,
    id: 2075663,
    country: 'IT',
    sunrise: 1661834187,
    sunset: 1661882248,
  },
  timezone: 7200,
  id: 3163858,
  name: 'Zocca',
  cod: 200,
};
type GeoCoords = {
  lon: number;
  lat: number;
};

let weatherSample: RealtimeWeather[] = [];
const currentTime = new Date();
currentTime.setFullYear(2023, 0, 1);
const maxValues = {
  maxTemperature: 300, // Maximum temperature in Kelvin
  maxPrecipitation: 16, // Maximum precipitation in mm
  maxWindSpeed: 12, // Maximum wind speed in m/s
  maxUVIndex: 7, // Maximum UV index
};
// fill initial sample
for (let i = 0; i < 12; i++) {
  const weather = generateMockWeatherData(
    currentTime,
    weatherSample[i - 1] || null,
    10,
    maxValues,
  );
  weatherSample.push(weather);
}
// keep updating the sample every 10 seconds
setInterval(() => {
  weatherSample.push(
    generateMockWeatherData(
      currentTime,
      weatherSample[weatherSample.length - 1],
      10000,
      maxValues,
    ),
  );
  weatherSample = weatherSample.slice(1);
}, 10000);
export const getWeather = (coord: GeoCoords): WeatherData => {
  return { realtime: weatherSample };
};
