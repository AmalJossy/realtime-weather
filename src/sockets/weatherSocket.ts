import { wsEvents } from '@/constants/socketEvents';
import { getWeather } from '@/services/server/weatherMock';
import type { Server, Socket } from 'socket.io';

export const registerWeatherSocket = (io: Server, socket: Socket) => {
  let interval: NodeJS.Timeout;
  socket.on(wsEvents.WEATHER_START, () => {
    if (interval) clearInterval(interval);
    interval = setInterval(() => {
      socket.emit(wsEvents.WEATHER_DATA, getWeather({ lat: 0, lon: 0 }));
    }, 10000);
  });
  socket.on(wsEvents.WEATHER_STOP, () => {
    clearInterval(interval);
  });
};
