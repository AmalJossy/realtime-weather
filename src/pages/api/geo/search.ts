import { popularCities } from '@/constants/popularCities';
import type { NextApiRequest, NextApiResponse } from 'next';
export default function handler(req: NextApiRequest, res: NextApiResponse) {
  const searchString = req.query.q;
  res.status(200).json({ results: popularCities });
}
