import { NextApiRequest, NextApiResponse } from 'next';

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse,
) {
  const { lat, lon } = req.query;
  const weatherRes = await fetch(
    `https://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${lon}&appid=06aac0fd4ba239a20d824ef89602f311`,
  );
  const weatherJson = await weatherRes.json();
  res.status(200).json(weatherJson);
}
