import { popularCities } from '@/constants/popularCities';
import { NextApiRequest, NextApiResponse } from 'next';

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse,
) {
  const cities = popularCities.slice(0, 5);
  const weatherRes = await Promise.all(
    cities.map((city) =>
      fetch(
        `https://api.openweathermap.org/data/2.5/weather?lat=${city.coordinates[0]}&lon=${city.coordinates[1]}&appid=06aac0fd4ba239a20d824ef89602f311`,
      ),
    ),
  );
  const weatherJson = await Promise.all(weatherRes.map((res) => res.json()));
  for (let i = 0; i < cities.length; i++) {
    weatherJson[i].city = cities[i].name;
    weatherJson[i].country = cities[i].country;
  }
  res.status(200).json(weatherJson);
}
