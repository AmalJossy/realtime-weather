import SocketProvider from '@/contexts/socketContext';
import Dashboard from '@/scenes/Dashboard';
import { Inter } from 'next/font/google';

const inter = Inter({ subsets: ['latin'] });

export default function Home() {
  return (
    <SocketProvider>
      <main
        className={`relative z-0 min-h-screen overflow-auto ${inter.className}`}
      >
        <Dashboard />
      </main>
    </SocketProvider>
  );
}
