import { TemperatureUnit, temperatureUnits } from '@/constants/weather';

export const kelvinToCelsius = (temp: number) => temp - 273;

export const kelvinToFahrenheit = (temp: number) => ((temp - 273) * 9) / 5 + 32;

type TemperatureDisplayOptions = {
  hasSign?: boolean;
  hasUnit?: boolean;
};
export const kelvinToCelsiusStr = (
  temp: number,
  options: TemperatureDisplayOptions = {},
) => {
  const value = Math.floor(kelvinToCelsius(temp));
  const prefix = options.hasSign && value >= 0 ? '+' : '';
  const suffix = options.hasUnit === false ? '°' : '°C';
  return `${prefix}${value}${suffix}`;
};

export const kelvinToFahrenheitStr = (
  temp: number,
  options: TemperatureDisplayOptions = {},
) => {
  const value = Math.floor(kelvinToFahrenheit(temp));
  const prefix = options.hasSign && value >= 0 ? '+' : '';
  const suffix = options.hasUnit === false ? '°' : '°F';
  return `${prefix}${value}${suffix}`;
};

export const kelvinToUnitStr = (
  temp: number,
  unit: TemperatureUnit,
  options?: TemperatureDisplayOptions,
) => {
  if (unit === temperatureUnits.FAHRENHEIT)
    return kelvinToFahrenheitStr(temp, options);
  return kelvinToCelsiusStr(temp, options);
};
